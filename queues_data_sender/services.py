import json
import logging
from typing import List, Dict, Any, Optional
from pyzabbix import ZabbixMetric

from db.DbManager import DbManager
from db.SqlBuilder import SqlBuilder
from utils.ZabbixManager import ZabbixManager
from utils.types import ZabbixConfig

logger = logging.getLogger()


class QueuesDataSenderService:
    def __init__(self, zabbix_config: ZabbixConfig, db_manager: DbManager, default_zabbix_item_key: str):
        self.zabbix_config = zabbix_config
        self.db_manager = db_manager
        self.default_zabbix_item_key = default_zabbix_item_key

    def _get_queue_data(self, db_name: str, companies_names: List[str]) -> List[Dict[str, Any]]:
        sql = SqlBuilder().build_queue_sql(companies_names=companies_names, db_name=db_name)
        result = self.db_manager.execute_sql(sql=sql)
        return result

    @staticmethod
    def _generate_zabbix_item_name(queue_name: str, company_name: str, db_name: str) -> str:
        return f'*MSSQL QUEUE: [{queue_name}] FROM DB: [{db_name}] FOR COMPANY: [{company_name}]*'

    def _get_zabbix_item_key(self, zabbix_hostname: str, queue_name: str, company_name: str, db_name: str,
                             default_item_key: str) \
            -> Optional[str]:
        zabbix_item_name = self._generate_zabbix_item_name(
            queue_name=queue_name,
            company_name=company_name,
            db_name=db_name
        )
        zabbix_item_key = ZabbixManager.find_item_name(
            search_item_name=zabbix_item_name,
            hostname=zabbix_hostname,
            zabbix_config=self.zabbix_config
        )
        if not zabbix_item_key:
            logger.info(f"Use default item with key '{default_item_key}'")
            zabbix_item_key = ZabbixManager.find_item(
                search_item_key=default_item_key,
                hostname=zabbix_hostname,
                zabbix_config=self.zabbix_config
            )
            if not zabbix_item_key:
                logger.error(f'Not found default zabbix item with key {default_item_key} for host {zabbix_hostname}')
                exit(-1)
        return zabbix_item_key

    @staticmethod
    def _parse_db_row_to_json(queue_data_row: Dict[str, Any]) -> str:
        data_dict = {
            "Firma": queue_data_row.get('Firma', ''),
            "Opis": queue_data_row.get('Opis', ''),
            "Stan": queue_data_row.get('Stan', ''),
            "Użytkownik": queue_data_row.get('Użytkownik', ''),
            "Zaplanowany": queue_data_row.get('Zaplanowany'),
            "Typ obiektu do uruchomienia": queue_data_row.get('Typ obiektu do uruchomienia', ''),
            "Identyfikator obiektu do uruchomienia": queue_data_row.get('Identyfikator obiektu do uruchomienia', ''),
            "Kod kategorii kolejki zleceń": queue_data_row.get('Kod kategorii kolejki zleceń', ''),
            "Najwcześniejsza data i godz rozpoczęcia": queue_data_row.get('Najwcześniejsza data i godz rozpoczęcia', ''),
            "Data i godzina rozpoczęcia": queue_data_row.get('Data i godzina rozpoczęcia', ''),
            "Komunikat o błędzie": queue_data_row.get('Komunikat o błędzie', '')
        }
        data_json = json.dumps(data_dict, ensure_ascii=False, default=str)
        return data_json

    def send_company_data(self, db_name: str, zabbix_hostname: str, companies_names: List[str]):
        queues_data_rows = self._get_queue_data(companies_names=companies_names, db_name=db_name)
        logger.info(f'Found {len(queues_data_rows)} rows in queue in db: {db_name}')
        if len(queues_data_rows):
            zabbix_queue_metrics: List[ZabbixMetric] = []
            for queue_data_row in queues_data_rows:
                queue_name = queue_data_row['Opis']
                company_name = queue_data_row['Firma']
                zabbix_item_key = self._get_zabbix_item_key(
                    zabbix_hostname=zabbix_hostname,
                    queue_name=queue_name,
                    company_name=company_name,
                    db_name=db_name,
                    default_item_key=self.default_zabbix_item_key
                )
                queue_data_json = self._parse_db_row_to_json(queue_data_row)
                if zabbix_item_key:
                    zabbix_metric = ZabbixMetric(
                        host=zabbix_hostname,
                        key=zabbix_item_key,
                        value=queue_data_json
                    )
                    zabbix_queue_metrics.append(zabbix_metric)
                else:
                    logger.warning('Not found item key by name ')
            ZabbixManager.send_metrics(zabbix_queue_metrics)
