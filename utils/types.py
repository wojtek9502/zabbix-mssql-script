import dataclasses


@dataclasses.dataclass
class DbConfig:
    db_name: str
    address: str
    port: str
    username: str
    password: str


@dataclasses.dataclass
class ZabbixConfig:
    ip: str
    port: str
    url: str
    api_user: str
    api_password: str
