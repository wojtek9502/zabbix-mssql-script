from typing import List, Dict

from pyzabbix import ZabbixMetric


def parse_metrics(metrics: List[ZabbixMetric]) -> Dict:
    metrics_data = dict()
    for metric in metrics:
        metrics_data[metric.key] = metric.value
    return metrics_data
