import logging
from typing import List, Optional

import pyzabbix
from pyzabbix import ZabbixMetric, ZabbixAPI

from utils.types import ZabbixConfig
from utils.utils import parse_metrics

logger = logging.getLogger()


class ZabbixException(Exception):
    ...


class ZabbixManager(pyzabbix.ZabbixSender):
    @staticmethod
    def find_item(search_item_key: str, hostname: str, zabbix_config: ZabbixConfig) -> Optional[str]:
        api = ZabbixAPI(url=zabbix_config.url, user=zabbix_config.api_user, password=zabbix_config.api_password)
        params = dict(
            output=["key_", 'hostid'],
            filter={
                "host": hostname
            },
            search={
                "key_": search_item_key,
            },
            searchWildcardsEnabled=True
        )
        items = api.do_request(
            method='item.get',
            params=params
        )
        results = items['result']
        if len(results) > 1:
            msg = f'Found more than 1 item by key "{search_item_key}" for host "{hostname}" in Zabbix'
            logger.error(msg)
            raise ZabbixException(msg)

        if len(results):
            item_key = results[0]['key_']
        else:
            item_key = None
            logger.warning(f'Not found item by name "{search_item_key}" for host "{hostname}"')
        return item_key

    @staticmethod
    def find_item_name(search_item_name: str, hostname: str, zabbix_config: ZabbixConfig) -> Optional[str]:
        api = ZabbixAPI(url=zabbix_config.url, user=zabbix_config.api_user, password=zabbix_config.api_password)
        params = dict(
            output=["key_", 'hostid'],
            filter={
                "host": hostname
            },
            search={
                "name": search_item_name,
            },
            searchWildcardsEnabled=True
        )
        items = api.do_request(
            method='item.get',
            params=params
        )
        results = items['result']
        if len(results) > 1:
            msg = f'Found more than 1 item by name "{search_item_name}" for host "{hostname}" in Zabbix'
            raise ZabbixException(msg)

        if len(results):
            item_key = results[0]['key_']
        else:
            item_key = None
            logger.warning(f'Not found item by name "{search_item_name}" for host "{hostname}"')
        return item_key

    @staticmethod
    def send_metric(metric: ZabbixMetric, zabbix_server: str = '127.0.0.1', zabbix_port: str = '10051'):
        metrics = [metric]
        result = pyzabbix.ZabbixSender(
            zabbix_server=zabbix_server,
            zabbix_port=int(zabbix_port)
        ).send(metrics)
        metrics_data_text = str(parse_metrics(metrics))
        if result.failed:
            msg = f"Zabbix sender error, failed {result.failed}, total {result.total}. Zabbix_server {zabbix_server}," \
                  f" port {zabbix_port}. Metrics: {metrics}"
            raise ZabbixException(msg)
        else:
            logger.info(f"Metrics sent to zabbix_server {zabbix_server} \n {metrics_data_text}")
        logger.info(
            f"Zabbix sender results, processed {result.processed}, failed {result.failed}, total {result.total}")

    @staticmethod
    def send_metrics(metrics: List[ZabbixMetric], zabbix_server: str = '127.0.0.1', zabbix_port: str = '10051'):
        result = pyzabbix.ZabbixSender(
            zabbix_server=zabbix_server,
            zabbix_port=int(zabbix_port)
        ).send(metrics)
        metrics_data_text = str(parse_metrics(metrics))
        if result.failed:
            msg = f"Zabbix sender error, failed {result.failed}, total {result.total}. Zabbix_server {zabbix_server}," \
                  f" port {zabbix_port}. Metrics: {metrics_data_text}"
            raise ZabbixException(msg)
        else:
            logger.info(f"Metrics sent to zabbix_server {zabbix_server} \n {metrics_data_text}")
        logger.info(
            f"Zabbix sender results, processed {result.processed}, failed {result.failed}, total {result.total}")
