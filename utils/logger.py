import logging
import sys
from pathlib import Path


def setup_logger(logger_level, log_file_path: Path):
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logger_level)

    logging.basicConfig(level=logging.DEBUG,
                        format='[%(asctime)s] [%(levelname)s] %(message)s',
                        handlers=[
                            logging.FileHandler(log_file_path, mode='a'),
                            stream_handler
                        ]
                        )

    logger = logging.getLogger()

    def my_handler(type, value, tb):
        logger.exception("Uncaught exception: {0}".format(str(tb.format_exc())))

    sys.excepthook = my_handler
