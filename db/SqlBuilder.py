from typing import List


class SqlBuilder:
    @staticmethod
    def _return_single_company_sql(company_name: str, db_name: str, add_union_all_statement: bool) -> str:
        company_name_var = company_name
        sql = f"""
                select
                '{company_name_var}' Firma
                , jqe.Description Opis
                , (case when jqe.Status = 0 then 'Ready' when jqe.Status = 1 then 'In Process' when jqe.Status = 2 then 'Error' when jqe.Status = 3 then 'On Hold' when jqe.Status = 4 then 'Finished' end) Stan
                , jqe.[User ID] [Użytkownik]
                , (case when exists (select st.ID from [{db_name}].[dbo].[Scheduled Task] st where st.ID = jqe.[System Task ID]) then 'Tak' else 'Nie' end) [Zaplanowany]
                , jqe.[Object Type to Run] [Typ obiektu do uruchomienia]
                , jqe.[Object ID to Run] [Identyfikator obiektu do uruchomienia]
                , jqe.[Job Queue Category Code] [Kod kategorii kolejki zleceń]
                , jqe.[Earliest Start Date_Time] [Najwcześniejsza data i godz rozpoczęcia]
                , jqle.[Start Date_Time] [Data i godzina rozpoczęcia]
                , jqle.[Error Message] [Komunikat o błędzie]
                from
                [{db_name}].[dbo].[JKB Polska$Job Queue Entry] jqe
                left outer join [{db_name}].[dbo].[JKB Polska$Job Queue Log Entry] jqle on jqle.ID = jqe.ID and jqle.[Job Queue Category Code] = jqe.[Job Queue Category Code]
                 and jqle.[Entry No_] >= (select MAX(jqle2.[Entry No_]) from [{db_name}].[dbo].[JKB Polska$Job Queue Log Entry] jqle2 where jqle2.[Object ID to Run] = jqe.[Object ID to Run] and jqle2.ID = jqe.ID and jqe.[Job Queue Category Code] = jqle2.[Job Queue Category Code])
                where
                jqe.[Earliest Start Date_Time] < jqle.[Start Date_Time]
                and exists (select st.ID from [{db_name}].[dbo].[Scheduled Task] st where st.ID = jqe.[System Task ID])
            """
        if add_union_all_statement:
            sql += f'\nunion all'
        sql += '\n'
        return sql

    def build_queue_sql(self, db_name: str, companies_names: List[str]) -> str:
        company_names_length = len(companies_names)
        sql = ''
        for i, company_name in enumerate(companies_names):
            if i == company_names_length - 1 or company_names_length == 1:
                sql += self._return_single_company_sql(
                    db_name=db_name,
                    company_name=company_name,
                    add_union_all_statement=False
                )
            else:
                sql += self._return_single_company_sql(
                    db_name=db_name,
                    company_name=company_name,
                    add_union_all_statement=True
                )
        return sql
