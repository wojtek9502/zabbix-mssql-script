import argparse
import logging
from pathlib import Path

from db.DbManager import DbManager
from queues_data_sender.services import QueuesDataSenderService
from utils.ConfigParser import ConfigParser
from utils.ZabbixManager import ZabbixManager
from utils.logger import setup_logger

PROJECT_DIR = Path(__file__).parent.resolve()
CONFIG_PATH = Path(PROJECT_DIR, 'config.yml')
MSSQL_DRIVER = '{ODBC Driver 17 for SQL Server}'
DEFAULT_ZABBIX_ITEM_KEY = 'mssql.queue.unknown.default'

# Scripts args parser
parser = argparse.ArgumentParser()
parser.add_argument("--hostname")
args = parser.parse_args()
hostname = args.hostname

# logger
LOGGER_LEVEL = logging.INFO
LOG_FILE_PATH = Path(PROJECT_DIR, 'logs.log')

setup_logger(logger_level=LOGGER_LEVEL, log_file_path=LOG_FILE_PATH)

logger = logging.getLogger()

COMPANIES_NAMES = [
    'company'
]

if __name__ == '__main__':
    logger.info(f'Hostname args: {hostname}')
    if not DEFAULT_ZABBIX_ITEM_KEY:
        logger.error('DEFAULT_ZABBIX_ITEM_KEY is empty or None. Check main.py')
        exit(-1)

    zabbix_config = ConfigParser.get_zabbix_api_config(
        config_path=str(CONFIG_PATH)
    )
    databases_configs = ConfigParser.get_db_config_by_hostname(
        config_path=str(CONFIG_PATH),
        hostname=hostname
    )

    for database_config in databases_configs:
        logger.info(f'Try to connect with db {database_config.db_name}')
        db_manager = DbManager(
            server=database_config.address,
            driver=MSSQL_DRIVER,
            db_name=database_config.db_name,
            username=database_config.username,
            password=database_config.password,
            encrypt=False,
            trust_server_cert=True
        )
        service = QueuesDataSenderService(
            zabbix_config=zabbix_config,
            db_manager=db_manager,
            default_zabbix_item_key=DEFAULT_ZABBIX_ITEM_KEY
        )
        service.send_company_data(
            db_name=database_config.db_name,
            zabbix_hostname=hostname,
            companies_names=COMPANIES_NAMES
        )
