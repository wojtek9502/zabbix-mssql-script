1. Create folder 'volumes'
```shell
mkdir volumes
sudo chmod 777 -R volumes
```

2. Change /var/run/docker.sock chown for zabbix agent
```shell
sudo chown 666 /var/run/docker.sock
```

3. Run containers
```shell
make up
make down
sudo chmod 777 -R volumes
make up
```

4. Go to zabbix web (http://localhost:8080) go to 'Zabbix server' configuration and change interface type on 'DNS' and fill 'Dns name' field with value 'zabbix-agent'. Zabbix host should be available now in Zabbix

5. Install requirements
```shell
make install
```