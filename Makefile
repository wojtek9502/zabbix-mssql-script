python = venv/bin/python
pip = venv/bin/pip

up:
	docker-compose up -d

pull:
	docker-compose pull

down:
	docker-compose down

update-requirements:
	$(pip) freeze > requirements.txt

uninstall-unrequired-libraries:
	$(pip) freeze | grep -v -f requirements.txt - | grep -v '^#' | xargs $(pip) uninstall -y || echo "OK, you dont have any unrequired libraries"

install: uninstall-unrequired-libraries
	$(pip) install -r requirements.txt