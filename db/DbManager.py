import logging
from typing import List, Dict

import pyodbc as pyodbc

logger = logging.getLogger()


class DbManager:
    def __init__(self, server: str, db_name: str, username: str, password: str,
                 driver: str,
                 encrypt: bool = True,
                 trust_server_cert: bool = False
                 ):

        db_uri = f"DRIVER={driver};SERVER={server};DATABASE={db_name};UID={username};PWD={password};"
        db_uri += self._get_additional_db_uri_params(encrypt=encrypt, trust_server_cert=trust_server_cert)
        try:
            self.db_conn: pyodbc.Connection = pyodbc.connect(db_uri)
            logger.info(f'connected to db {db_name}, server: server {server}')
        except pyodbc.Error as e:
            self.db_conn = None
            logger.error(f'Db connection, db uri: "{db_uri}". Error: {str(e)}')
            exit(-1)

    def __del__(self):
        if self.db_conn:
            try:
                self.db_conn.close()
            except pyodbc.Error as e:
                logger.error(f'Db connection close error: {str(e)}')
                exit(-1)

    @staticmethod
    def _get_additional_db_uri_params(encrypt: bool, trust_server_cert: bool) -> str:
        db_uri_params = ''
        if encrypt:
            db_uri_params += 'encrypt=YES;'
        else:
            db_uri_params += 'encrypt=NO;'

        if trust_server_cert:
            db_uri_params += 'TrustServerCertificate=YES;'
        return db_uri_params

    def commit(self):
        try:
            self.db_conn.commit()
        except pyodbc.Error as e:
            logger.error(f'Db commit error: {str(e)}')
            exit(-1)

    def execute_sql(self, sql: str) -> List[Dict]:
        results: List[Dict] = []
        cursor: pyodbc.Cursor = self.db_conn.cursor()
        try:
            cursor.execute(sql)
        except pyodbc.Error as e:
            logger.error(f'Db execute sql error: {str(e)}')
            logger.debug(f'SQL:\n"{sql}"')
            exit(-1)

        columns = [column[0] for column in cursor.description]
        for row in cursor.fetchall():
            results.append(dict(zip(columns, row)))

        cursor.close()
        return results
