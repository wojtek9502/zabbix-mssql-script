import logging
from typing import Dict, List

import yaml
from yaml import SafeLoader

from utils.types import DbConfig, ZabbixConfig

logger = logging.getLogger()


class ConfigParser:
    @staticmethod
    def _load_config(config_path: str) -> Dict:
        with open(config_path, 'r') as f:
            config_data = yaml.load(f, Loader=SafeLoader)
        return config_data

    @staticmethod
    def _get_single_db_config(db_config: Dict):
        for db_name, db_config in db_config.items():
            db_config_obj = DbConfig(
                db_name=db_name,
                address=db_config['address'],
                port=db_config['port'],
                username=db_config['user'],
                password=db_config['password']
            )
            return db_config_obj

    @classmethod
    def get_zabbix_api_config(cls, config_path: str) -> ZabbixConfig:
        config_data = cls._load_config(config_path)
        zabbix_config = config_data.get('zabbix', None)
        if not zabbix_config:
            logger.error(f'Not found zabbix credentials in config. Check config.yml file')
            exit(-1)

        return ZabbixConfig(
            ip=zabbix_config['ip'],
            port=zabbix_config['port'],
            url=zabbix_config['url'],
            api_user=zabbix_config['api_user'],
            api_password=zabbix_config['api_password']
        )


    @classmethod
    def get_db_config_by_hostname(cls, config_path: str, hostname: str) -> List[DbConfig]:
        db_configs = []
        config_data = cls._load_config(config_path)
        hostnames_config = config_data.get('zabbix_hosts', None)
        if hostname not in hostnames_config.keys():
            logger.error(f'Not found host {hostname} config. Check config.yml file')
            exit(-1)

        databases_config_raw_data = hostnames_config[hostname]
        for db_config_dict in databases_config_raw_data:
            db_config = cls._get_single_db_config(db_config_dict)
            db_configs.append(db_config)
        return db_configs
